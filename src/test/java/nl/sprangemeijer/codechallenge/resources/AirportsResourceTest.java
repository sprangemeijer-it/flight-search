package nl.sprangemeijer.codechallenge.resources;

import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import io.dropwizard.testing.junit5.ResourceExtension;
import nl.sprangemeijer.codechallenge.core.Airport;
import nl.sprangemeijer.codechallenge.db.Airports;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(DropwizardExtensionsSupport.class)
class AirportsResourceTest {

    private static final ResourceExtension RESOURCES = ResourceExtension.builder()
            .addResource(new AirportsResource())
            .build();

    @Test
    void allAirports() {
        final Response response = RESOURCES.target("/airports")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.OK);

        final List<Airport> airports = response.readEntity(new GenericType<>() {});
        assertThat(airports).isNotNull().hasSize(3);
        assertThat(airports).contains(Airports.get("AMS"), Airports.get("NCE"), Airports.get("FCO"));
    }
}
