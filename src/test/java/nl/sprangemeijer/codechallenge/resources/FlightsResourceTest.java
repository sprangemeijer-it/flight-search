package nl.sprangemeijer.codechallenge.resources;

import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import io.dropwizard.testing.junit5.ResourceExtension;
import io.reactivex.Flowable;
import nl.sprangemeijer.codechallenge.core.Flight;
import nl.sprangemeijer.codechallenge.core.FlightClass;
import nl.sprangemeijer.codechallenge.core.FlightSearchQuery;
import nl.sprangemeijer.codechallenge.core.FlightsSearchService;
import nl.sprangemeijer.codechallenge.db.Flights;
import org.glassfish.jersey.test.grizzly.GrizzlyWebTestContainerFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.concurrent.TimeUnit;

import static nl.sprangemeijer.codechallenge.db.Airports.AMS;
import static nl.sprangemeijer.codechallenge.db.Airports.FCO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(DropwizardExtensionsSupport.class)
class FlightsResourceTest {

    final FlightsSearchService flightsSearchService = Mockito.mock(FlightsSearchService.class);

    private final ResourceExtension RESOURCES = ResourceExtension.builder()
            .addResource(new FlightsResource(flightsSearchService))
            .setTestContainerFactory(new GrizzlyWebTestContainerFactory())
            .build();

    @Test
    void rxSearchOneWayFlight() {
        Flowable<Flight> flights = Flowable.just(Flights.createOneWayFlight("easyJet", AMS, FCO, "2021-10-01T09:00:00", "2021-10-01T11:00:00", 1, FlightClass.ECONOMY));
        when(flightsSearchService.rxSearchFlights(any(FlightSearchQuery.class))).thenReturn(flights);

        final Response response = RESOURCES.target("/flights/rx-search-one-way")
                .queryParam("origin", AMS)
                .queryParam("destination", FCO)
                .queryParam("startDate", LocalDate.parse("2021-10-01"))
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.OK);
    }

    @Test
    void rxSearchOneWayFlightStreaming() {
        Flowable<Flight> flights = Flowable.just(
                Flights.createOneWayFlight("easyJet", AMS, FCO, "2021-10-01T09:00:00", "2021-10-01T11:00:00", 1, FlightClass.ECONOMY),
                Flights.createOneWayFlight("KLM", AMS, FCO, "2021-10-01T09:00:00", "2021-10-01T11:00:00", 1, FlightClass.ECONOMY),
                Flights.createOneWayFlight("Vueling", AMS, FCO, "2021-10-01T09:00:00", "2021-10-01T11:00:00", 1, FlightClass.ECONOMY));
        when(flightsSearchService.rxSearchFlights(any(FlightSearchQuery.class))).thenReturn(flights.delay(500, TimeUnit.MILLISECONDS));

        final Response response = RESOURCES.target("/flights/rx-search-one-way-streaming")
                .queryParam("origin", "AMS")
                .queryParam("destination", "FCO")
                .queryParam("startDate", LocalDate.parse("2021-10-01"))
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.OK);

        // TODO: add validation of incoming Flights...
    }

    @Test
    void rxSearchRoundTrip() {
        Flowable<Flight> flights = Flowable.just(Flights.createRoundTripFlight("easyJet", AMS, FCO, "2021-10-01T09:00:00", "2021-10-03T11:00:00", 1, FlightClass.ECONOMY));
        when(flightsSearchService.rxSearchFlights(any(FlightSearchQuery.class))).thenReturn(flights);

        final Response response = RESOURCES.target("/flights/rx-search-one-way")
                .queryParam("origin", AMS)
                .queryParam("destination", FCO)
                .queryParam("startDate", LocalDate.parse("2021-10-01"))
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        assertThat(response.getStatusInfo()).isEqualTo(Response.Status.OK);
    }
}
