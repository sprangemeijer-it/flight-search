package nl.sprangemeijer.codechallenge.core;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.time.LocalDate;

/**
 * Query object to specify the properties to which the requested flights should conform.
 * @param originCode IATA code of origin airport
 * @param destinatonCode IATA code of destination airport
 * @param startDate LocalDate on which a flight should start
 * @param endDate Localdate on which a flight should end
 * @param nrOfPassengers Optional Number of passengers, defaulting to 1
 * @param directFlightsOnly Optonal indication that you only want direct flights, no multi stop flights, default false
 * @param flightType Type of flight, defaulting to FlightType.ROUND_TRIP
 * @param flightClass Class of flight, defaulting to FlightClass.ECONOMY
 */
@Builder
@Value
public class FlightSearchQuery {
  @NonNull String originCode;
  @NonNull String destinationCode;
  LocalDate startDate;
  LocalDate endDate;
  @Builder.Default
  int nrOfPassengers = 1;
  @Builder.Default
  boolean directFlightsOnly = false;

  @Builder.Default
  FlightType flightType = FlightType.ROUND_TRIP;
  @Builder.Default
  FlightClass flightClass = FlightClass.ECONOMY;
}
