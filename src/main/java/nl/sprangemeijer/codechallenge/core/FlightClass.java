package nl.sprangemeijer.codechallenge.core;

/**
 * Enumeration of supported FlightClass types.
 */
public enum FlightClass {
    ECONOMY,
    PREMIUM_ECONOMY,
    BUSINESS_CLASS,
    FIRST_CLASS
}
