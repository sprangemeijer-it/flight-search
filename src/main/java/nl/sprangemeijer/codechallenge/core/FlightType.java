package nl.sprangemeijer.codechallenge.core;

/**
 * Enumeration of supported FlightType types.
 */
public enum FlightType {
  ONE_WAY,
  ROUND_TRIP,
  MULTI_CITY
}
