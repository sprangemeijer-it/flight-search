package nl.sprangemeijer.codechallenge.core;

import io.reactivex.Flowable;

public interface ExternalFlightService {
    /**
     * Search for flights in a reactive manner.
     * Subscribe to the resulting Flowable and process the returned Flights or errors.
     * @param query parameter object to specify properties to which requested flights should conform
     * @return Flowable of Flight objects
     */
    Flowable<Flight> rxSearchFlights(FlightSearchQuery query);
}
