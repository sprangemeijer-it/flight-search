package nl.sprangemeijer.codechallenge.core;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * A FlightLeg is a part of a Flight, connection a destination Airport with an Arrival Airport.
 * Leaving at a certain moment in time and arriving at some point later in time.
 */
public class FlightLeg {

  private final Airport departFrom;
  private final LocalDateTime departOn;
  private final Airport arriveAt;
  private final LocalDateTime arriveOn;
  private final Double price;
  private final FlightClass flightClass;

  @JsonCreator
  public FlightLeg(@JsonProperty("departFrom")
                     Airport departFrom,
                   @JsonProperty("departOn")
                   @JsonDeserialize(using = LocalDateTimeDeserializer.class)
                   @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
                     LocalDateTime departOn,
                   @JsonProperty("arriveAt")
                     Airport arriveAt,
                   @JsonProperty("arriveOn")
                   @JsonDeserialize(using = LocalDateTimeDeserializer.class)
                   @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
                     LocalDateTime arriveOn,
                   @JsonProperty("price")
                     Double price,
                   @JsonProperty("flightClass")
                     FlightClass flightClass
                   ) {
    this.departFrom = Objects.requireNonNull(departFrom, "departFrom cannot be null");
    this.departOn = Objects.requireNonNull(departOn, "departOn cannot be null");
    this.arriveAt = Objects.requireNonNull(arriveAt, "arriveAt cannot be null");
    this.arriveOn = Objects.requireNonNull(arriveOn, "arriveOn cannot be null");
    this.price = Objects.requireNonNull(price, "price cannot be null");
    this.flightClass = Objects.requireNonNull(flightClass, "flightClass cannot be null");
  }

  public Airport getDepartFrom() {
    return departFrom;
  }

  public LocalDateTime getDepartOn() {
    return departOn;
  }

  public Airport getArriveAt() {
    return arriveAt;
  }

  public LocalDateTime getArriveOn() {
    return arriveOn;
  }

  public Double getPrice() {
    return price;
  }

  public FlightClass getFlightClass() {
    return flightClass;
  }
}
