package nl.sprangemeijer.codechallenge.core;

import io.reactivex.Flowable;

/**
 * Service that lets you query for flights conforming to specified query parameters.
 */
public interface FlightsSearchService {

  /**
   * Search for flights in a reactive manner.
   * Subscribe to the resulting Flowable and process the returned Flights or errors.
   * @param query parameter object to specify properties to which requested flights should conform
   * @return Flowable of Flight objects
   */
  Flowable<Flight> rxSearchFlights(FlightSearchQuery query);
}
