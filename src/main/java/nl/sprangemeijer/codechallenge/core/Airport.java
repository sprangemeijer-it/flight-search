package nl.sprangemeijer.codechallenge.core;

/**
 * Simple representation of an Airport, immutable.
 * @param name The name of the airport
 * @param code The IATA code of the airport
 * @param city The city where the airport is located
 */
public record Airport(String name, String code, String city) { }
