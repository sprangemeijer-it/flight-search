package nl.sprangemeijer.codechallenge.core;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;

/**
 * A Flight, consisting of at least 1 FlightLeg. A total-cost depending on the price of the legs and the number of passengers.
 */
public class Flight {

  private final String airline;
  private final String flightNumber;
  private final List<FlightLeg> legs;

  private final int nrOfPassengers;
  private final BigDecimal totalCosts;

  public Flight(String airline,
                String flightNumber,
                List<FlightLeg> legs,
                int nrOfPassengers) {

    this.airline = Objects.requireNonNull(airline, "airline cannot be null");
    this.flightNumber = Objects.requireNonNull(flightNumber, "flightNumber cannot be null");

    this.legs = Objects.requireNonNull(legs, "legs cannot be null");
    if(legs.isEmpty()) {
      throw new IllegalArgumentException("Flights should have at least 1 leg");
    }

    if(nrOfPassengers < 1) {
      throw new IllegalArgumentException("nrOfPassengers should be at least 1.");
    }
    this.nrOfPassengers = nrOfPassengers;

    var departOn = getFirstLeg().getDepartOn();
    var arriveOn = getLastLeg().getArriveOn();
    if(arriveOn.isBefore(departOn)) {
      throw new IllegalArgumentException("Departure must be before arrival");
    }

    final double totalPriceOfLegs = legs.stream().mapToDouble(FlightLeg::getPrice).sum();
    this.totalCosts = new BigDecimal(totalPriceOfLegs * nrOfPassengers).setScale(2, RoundingMode.HALF_UP);
  }

  public String getAirline() {
    return airline;
  }

  public String getFlightNumber() {
    return flightNumber;
  }

  public List<FlightLeg> getLegs() {
    return legs;
  }

  public BigDecimal getTotalCosts() {
    return totalCosts;
  }

  public int getNrOfPassengers() {
    return nrOfPassengers;
  }

  @JsonIgnore
  public boolean isNonStop() {
    return legs.size() == 1;
  }

  private FlightLeg getFirstLeg() {
    return legs.get(0);
  }

  @JsonIgnore
  public Airport getDepartFrom() {
    return getFirstLeg().getDepartFrom();
  }

  private FlightLeg getLastLeg() {
    return legs.get(legs.size() - 1);
  }

  @JsonIgnore
  public Airport getArriveAt() {
    return getLastLeg().getArriveAt();
  }

  @JsonIgnore
  public int getNumberOfLegs() {
    return legs.size();
  }

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  public Duration getTotalTravelTime() {
    var departure = getFirstLeg().getDepartOn();
    var arrival = getLastLeg().getArriveOn();

    return Duration.of(ChronoUnit.SECONDS.between(departure, arrival), ChronoUnit.SECONDS);
  }
}
