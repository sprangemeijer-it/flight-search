package nl.sprangemeijer.codechallenge.db.stubs;

import io.reactivex.Flowable;
import lombok.extern.slf4j.Slf4j;
import nl.sprangemeijer.codechallenge.core.ExternalFlightService;
import nl.sprangemeijer.codechallenge.core.Flight;
import nl.sprangemeijer.codechallenge.core.FlightSearchQuery;
import nl.sprangemeijer.codechallenge.db.Flights;

import java.util.concurrent.TimeUnit;

@Slf4j
public class KLMFlightService implements ExternalFlightService {
    @Override
    public Flowable<Flight> rxSearchFlights(FlightSearchQuery query) {

        Flight flight1 = Flights.createOneWayFlight("KLM", query.getOriginCode(), query.getDestinationCode(),  "2021-10-01T09:20:00", "2021-10-01T11:20:00", query.getNrOfPassengers(), query.getFlightClass());
        Flight flight2 = Flights.createOneWayFlight("KLM", query.getOriginCode(), query.getDestinationCode(), "2021-10-01T10:20:00", "2021-10-01T12:20:00", query.getNrOfPassengers(), query.getFlightClass());
        Flight flight3 = Flights.createOneWayFlight("KLM", query.getOriginCode(), query.getDestinationCode(), "2021-10-01T11:20:00", "2021-10-01T13:20:00", query.getNrOfPassengers(), query.getFlightClass());
        Flight flight4 = Flights.createOneWayFlight("KLM", query.getOriginCode(), query.getDestinationCode(), "2021-10-01T12:20:00", "2021-10-01T14:20:00", query.getNrOfPassengers(), query.getFlightClass());
        Flight flight5 = Flights.createOneWayFlight("KLM", query.getOriginCode(), query.getDestinationCode(), "2021-10-01T13:20:00", "2021-10-01T15:20:00", query.getNrOfPassengers(), query.getFlightClass());
        Flight flight6 = Flights.createOneWayFlight("KLM", query.getOriginCode(), query.getDestinationCode(), "2021-10-01T14:20:00", "2021-10-01T16:20:00", query.getNrOfPassengers(), query.getFlightClass());

        Flowable<Flight> flights = Flowable.just(flight1, flight2, flight3, flight4, flight5, flight6);

        return flights
                .delay(1, TimeUnit.SECONDS)
                .doOnNext(flight -> log.debug("flight: {}: {}", flight.getAirline(), flight.getFlightNumber()))
                .doOnSubscribe(subscription -> log.info("onSubscribe"));
    }}
