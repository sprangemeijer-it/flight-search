package nl.sprangemeijer.codechallenge.db;

import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.operator.CircuitBreakerOperator;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.transformer.RetryTransformer;
import io.reactivex.Flowable;
import lombok.extern.slf4j.Slf4j;
import nl.sprangemeijer.codechallenge.core.ExternalFlightService;
import nl.sprangemeijer.codechallenge.core.Flight;
import nl.sprangemeijer.codechallenge.core.FlightSearchQuery;
import nl.sprangemeijer.codechallenge.core.FlightsSearchService;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class FlightSearchFacade implements FlightsSearchService {

    private final List<ExternalFlightService> externalFlightServices;

    public FlightSearchFacade(ExternalFlightService... externalFlightServices) {
        this.externalFlightServices = List.of(externalFlightServices);
    }

    @Override
    public Flowable<Flight> rxSearchFlights(FlightSearchQuery query) {
        // call all external systems and collect and combine info
        // use circuit breaker / etc

        log.debug("rxSearchFlights called with query: {}", query);

        final List<Flowable<Flight>> listOfFlowables = externalFlightServices.stream()
                .map(externalFlightService -> {
                    final String name = externalFlightService.getClass().getSimpleName();
                    final CircuitBreaker circuitBreaker = CircuitBreaker.ofDefaults(name);
                    final Retry retry = Retry.ofDefaults(name);
                    return externalFlightService.rxSearchFlights(query)
                            .compose(RetryTransformer.of(retry))
                            .compose(CircuitBreakerOperator.of(circuitBreaker));
                })
                .collect(Collectors.toList());

        return Flowable.mergeDelayError(listOfFlowables);
    }
}
