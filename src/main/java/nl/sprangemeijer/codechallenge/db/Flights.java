package nl.sprangemeijer.codechallenge.db;

import nl.sprangemeijer.codechallenge.core.Flight;
import nl.sprangemeijer.codechallenge.core.FlightClass;
import nl.sprangemeijer.codechallenge.core.FlightLeg;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public class Flights {

    public static Flight createOneWayFlight(String airline, String departFrom, String arriveAt, String departOn, String arriveOn, int nrOfPassengers, FlightClass flightClass) {
        var flightNumber = UUID.randomUUID().toString();
        var price = 100.0;

        final FlightLeg outwardLeg = new FlightLeg(
                Airports.get(departFrom),
                LocalDateTime.parse(departOn),
                Airports.get(arriveAt),
                LocalDateTime.parse(arriveOn),
                price,
                flightClass
        );
        return new Flight(airline, flightNumber, List.of(outwardLeg), nrOfPassengers);
    }

    public static Flight createRoundTripFlight(String airline, String departFrom, String arriveAt, String departOnStr, String arriveOnStr, int nrOfPassengers, FlightClass flightClass) {
        var flightNumber = UUID.randomUUID().toString();
        var price = 100.0;

        LocalDateTime departOn = LocalDateTime.parse(departOnStr);
        final FlightLeg outwardLeg = new FlightLeg(
                Airports.get(departFrom),
                departOn,
                Airports.get(arriveAt),
                departOn.plusHours(2),
                price,
                flightClass
        );

        LocalDateTime arriveOn = LocalDateTime.parse(arriveOnStr);
        final FlightLeg returnLeg = new FlightLeg(
                outwardLeg.getArriveAt(),
                arriveOn,
                outwardLeg.getDepartFrom(),
                arriveOn.plusHours(2),
                price,
                flightClass
        );
        return new Flight(airline, flightNumber, List.of(outwardLeg, returnLeg), nrOfPassengers);
    }

}
