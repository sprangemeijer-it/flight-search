package nl.sprangemeijer.codechallenge.db;

import nl.sprangemeijer.codechallenge.core.Airport;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Airports {

    public static String AMS = "AMS";
    public static String FCO = "FCO";
    public static String NCE = "NCE";

    private static final Map<String, Airport> airportsByCode = Map.of(
            AMS, new Airport("Amsterdam Schiphol Airport", AMS, "Amsterdam"),
            FCO, new Airport("Leonardo Da Vinci Fiumicino Airport", FCO, "Rome"),
            NCE, new Airport("Nice-Cote D'Azur Airport", NCE, "Nice")
    );

    public static Airport get(final String code) {
        return airportsByCode.get(code);
    }

    public static List<Airport> getAll() {
        return new ArrayList<>(airportsByCode.values());
    }
}
