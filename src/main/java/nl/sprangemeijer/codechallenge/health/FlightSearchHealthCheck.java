package nl.sprangemeijer.codechallenge.health;

import com.codahale.metrics.health.HealthCheck;

public class FlightSearchHealthCheck extends HealthCheck {

  @Override
  protected Result check() throws Exception {
    // check required APIs and/or Databases etc...

    return Result.healthy();
  }
}
