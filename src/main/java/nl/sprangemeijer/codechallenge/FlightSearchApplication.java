package nl.sprangemeijer.codechallenge;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import nl.sprangemeijer.codechallenge.core.FlightsSearchService;
import nl.sprangemeijer.codechallenge.db.stubs.EasyJetFlightService;
import nl.sprangemeijer.codechallenge.db.FlightSearchFacade;
import nl.sprangemeijer.codechallenge.db.stubs.KLMFlightService;
import nl.sprangemeijer.codechallenge.db.stubs.VuelingFlightService;
import nl.sprangemeijer.codechallenge.health.FlightSearchHealthCheck;
import nl.sprangemeijer.codechallenge.resources.AirportsResource;
import nl.sprangemeijer.codechallenge.resources.FlightsResource;
import org.glassfish.jersey.server.ServerProperties;

public class FlightSearchApplication extends Application<FlightSearchConfiguration> {

    public static void main(final String[] args) throws Exception {
        new FlightSearchApplication().run(args);
    }

    @Override
    public String getName() {
        return "FlightSearch";
    }

    @Override
    public void initialize(final Bootstrap<FlightSearchConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final FlightSearchConfiguration configuration,
                    final Environment environment) {
        environment.jersey().property(ServerProperties.OUTBOUND_CONTENT_LENGTH_BUFFER, 0);
        final AirportsResource airportsResource = new AirportsResource();
        environment.jersey().register(airportsResource);

        final FlightsSearchService flightsSearchService = new FlightSearchFacade(new EasyJetFlightService(), new KLMFlightService(), new VuelingFlightService());
        final FlightsResource flightsResource = new FlightsResource(flightsSearchService);
        environment.jersey().register(flightsResource);

        final FlightSearchHealthCheck healthCheck = new FlightSearchHealthCheck();
        environment.healthChecks().register("APIHealthCheck", healthCheck);
    }
}
