package nl.sprangemeijer.codechallenge.resources;

import com.codahale.metrics.annotation.Timed;
import nl.sprangemeijer.codechallenge.core.Airport;
import nl.sprangemeijer.codechallenge.db.Airports;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/airports")
public class AirportsResource {
  @GET
  @Timed
  @Produces(MediaType.APPLICATION_JSON)
  public List<Airport> allAirports() {
    return Airports.getAll();
  }
}
