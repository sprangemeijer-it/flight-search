package nl.sprangemeijer.codechallenge.resources;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectWriter;
import io.dropwizard.jackson.Jackson;
import io.dropwizard.jersey.jsr310.LocalDateParam;
import io.reactivex.schedulers.Schedulers;
import lombok.extern.slf4j.Slf4j;
import nl.sprangemeijer.codechallenge.core.Flight;
import nl.sprangemeijer.codechallenge.core.FlightClass;
import nl.sprangemeijer.codechallenge.core.FlightSearchQuery;
import nl.sprangemeijer.codechallenge.core.FlightType;
import nl.sprangemeijer.codechallenge.core.FlightsSearchService;
import org.glassfish.jersey.server.ChunkedOutput;

import javax.validation.constraints.Min;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

@Path("/flights")
@Slf4j
public class FlightsResource {
    private final FlightsSearchService flightsSearchService;

    public FlightsResource(final FlightsSearchService flightsSearchService) {
        this.flightsSearchService = flightsSearchService;
    }

    @GET
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/rx-search-one-way")
    public void rxSearchOneWayFlight(@Suspended final AsyncResponse asyncResponse,
                                     @QueryParam("origin") String origin,
                                     @QueryParam("destination") String destination,
                                     @QueryParam("startDate") LocalDateParam start,
                                     @Min(value = 1, message = "Nr of passengers must be 1 or greater") @QueryParam("nrOfPassengers") @DefaultValue("1") int nrOfPassengers,
                                     @QueryParam("directFlightsOnly") @DefaultValue("false") boolean directFlightsOnly,
                                     @QueryParam("flightClass") Optional<FlightClass> flightClass) {

        var flightSearchQuery = FlightSearchQuery.builder()
                .originCode(origin)
                .destinationCode(destination)
                .startDate(start.get())
                .nrOfPassengers(nrOfPassengers)
                .directFlightsOnly(directFlightsOnly)
                .flightType(FlightType.ONE_WAY)
                .flightClass(flightClass.orElse(FlightClass.ECONOMY))
                .build();
        log.debug("flightSearchQuery: {}", flightSearchQuery);

        flightsSearchService.rxSearchFlights(flightSearchQuery)
                .toList()
                .subscribe(
                        flights -> asyncResponse.resume(Response.ok(flights).build()),
                        throwable -> asyncResponse.resume(Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), throwable.toString()).build())
                );
    }

    @GET
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/rx-search-round-trip")
    public void rxSearchRoundTrip(@Suspended final AsyncResponse asyncResponse,
                                     @QueryParam("origin") String origin,
                                     @QueryParam("destination") String destination,
                                  @QueryParam("startDate") LocalDateParam start,
                                  @QueryParam("endDate") LocalDateParam end,
                                     @Min(value = 1, message = "Nr of passengers must be 1 or greater") @QueryParam("nrOfPassengers") @DefaultValue("1") int nrOfPassengers,
                                     @QueryParam("directFlightsOnly") @DefaultValue("false") boolean directFlightsOnly,
                                     @QueryParam("flightClass") Optional<FlightClass> flightClass) {

        var flightSearchQuery = FlightSearchQuery.builder()
                .originCode(origin)
                .destinationCode(destination)
                .startDate(start.get())
                .endDate(end.get())
                .nrOfPassengers(nrOfPassengers)
                .directFlightsOnly(directFlightsOnly)
                .flightType(FlightType.ROUND_TRIP)
                .flightClass(flightClass.orElse(FlightClass.ECONOMY))
                .build();
        log.debug("flightSearchQuery: {}", flightSearchQuery);

        flightsSearchService.rxSearchFlights(flightSearchQuery)
                .toList()
                .subscribe(
                        flights -> asyncResponse.resume(Response.ok(flights).build()),
                        throwable -> asyncResponse.resume(Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), throwable.toString()).build())
                );
    }


    @GET
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/rx-search-one-way-streaming")
    public ChunkedOutput<String> rxSearchOneWayFlightStreaming(
                                                               @QueryParam("origin") String origin,
                                                               @QueryParam("destination") String destination,
                                                               @QueryParam("startDate") LocalDateParam start,
                                                               @Min(value = 1, message = "Nr of passengers must be 1 or greater") @QueryParam("nrOfPassengers") @DefaultValue("1") int nrOfPassengers,
                                                               @QueryParam("directFlightsOnly") @DefaultValue("false") boolean directFlightsOnly,
                                                               @QueryParam("flightClass") Optional<FlightClass> flightClass) {

        var flightSearchQuery = FlightSearchQuery.builder()
                .originCode(origin)
                .destinationCode(destination)
                .startDate(start.get())
                .nrOfPassengers(nrOfPassengers)
                .directFlightsOnly(directFlightsOnly)
                .flightType(FlightType.ONE_WAY)
                .flightClass(flightClass.orElse(FlightClass.ECONOMY))
                .build();
        log.debug("flightSearchQuery: {}", flightSearchQuery);

        final ObjectWriter objectWriter = Jackson.newObjectMapper().disable(JsonGenerator.Feature.AUTO_CLOSE_JSON_CONTENT).writerFor(Flight.class);

        final ChunkedOutput<String> output = new ChunkedOutput<>(String.class, ",");
        try {
            flightsSearchService.rxSearchFlights(flightSearchQuery)
                    .doOnNext(flight -> log.debug("flight: {}, {}", flight.getAirline(), flight.getFlightNumber()))
                    .map(objectWriter::writeValueAsString)
                    .subscribeOn(Schedulers.io())
                    .subscribe(output::write,
                            throwable -> log.error("throwable: ", throwable),
                            () -> {
                                log.info("onComplete");
                                output.close();
                            }
                    );
        } catch (Exception e) {
            log.error("Caught an Exception", e);
        }
        return output;
    }
}
