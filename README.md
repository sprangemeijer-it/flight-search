# FlightSearch

How to start the FlightSearch application
---

1. Run `mvn clean install` to build your application (specify`-Ddependency-check.skip` to skip the OWASP dependency checker)
1. Start application with `java -jar target/flightsearch-1.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080`

Health Check
---

To see your applications health enter url `http://localhost:8081/healthcheck`
